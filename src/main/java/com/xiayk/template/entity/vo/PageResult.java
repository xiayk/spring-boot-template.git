package com.xiayk.template.entity.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xiayk.template.entity.param.BaseQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.function.Function;

public class PageResult<T> extends PageImpl<T> {

    private long total;

    private long totalPages;

    private List<T> rows;

    @JsonIgnore
    @Override
    public int getNumber() {
        return super.getNumber();
    }

    @JsonIgnore
    @Override
    public List<T> getContent() {
        return super.getContent();
    }

    public <Q extends BaseQuery> PageResult(List<T> rows, long total, Q q){
        super(rows, q.getPage(), total);
        this.rows = rows;
        this.total = total;
        this.totalPages = this.getTotalPages();
    }

    public PageResult(Page<T> page){
        super(page.getContent(), page.getPageable(), page.getTotalElements());
        this.rows = page.getContent();
        this.total = page.getTotalElements();
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public PageResult(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    @Override
    public String toString() {
        return "PageResult{" +
                "total=" + total +
                ", totalPages=" + totalPages +
                ", rows=" + rows +
                '}';
    }
}
