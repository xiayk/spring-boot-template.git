package com.xiayk.template.entity.form;

import com.xiayk.template.common.utils.ReflectionUtils;
import com.xiayk.template.common.utils.StringUtil;
import org.springframework.lang.NonNull;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class BaseForm extends HashMap<String, Object> {

    @Override
    @NonNull
    public BaseForm put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public String getString(String key){
        Object val = super.get(key);
        if (val == null){
            return null;
        }
        return val.toString();
    }

    public Integer getInteger(String key){
        Object val = super.get(key);
        if (val == null){
            return null;
        }
        return Integer.valueOf(val.toString());
    }

    public Long getLong(String key){
        Object val = super.get(key);
        if (val == null){
            return null;
        }
        return Long.valueOf(val.toString());
    }

    public <T> T toBean(Class<T> clazz) throws Exception{
        //创建泛型对象
        T entity = clazz.newInstance();
        //获取泛型属性名
        List<String> names = Arrays.stream(clazz.getDeclaredFields()).map(Field::getName).collect(Collectors.toList());
        //遍历HashMap
        this.forEach((k ,v) -> {
            if (names.contains(k)){
                ReflectionUtils.invoke(entity, "set" + StringUtil.firstLetterToUpperCase(k), v);
            }
        });
        return entity;
    }
}