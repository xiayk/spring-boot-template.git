package com.xiayk.template.common.exception;

public class EntityNotFountException extends BaseException{

    public EntityNotFountException(String message) {
        super(4004, message);
    }
}
