package com.xiayk.template.common;

import com.xiayk.template.common.utils.SpringContextHelper;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author XiaYk
 * @date 2021/9/13 15:55
 */
public class SecondApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        new SpringContextHelper(applicationContext);
    }
}