package com.xiayk.template.common.utils;

public class StringUtil {

    public static String firstLetterToLowerCase(String str){
        if (str != null && str.length() > 0){
            char[] chars = str.toCharArray();
            chars[0] = (char) (chars[0] >= 65 && chars[0] <= 90 ? chars[0] + 32 : chars[0]);
            return String.valueOf(chars);
        }
        return null;
    }

    public static String firstLetterToUpperCase(String str){
        if (str != null && str.length() > 0){
            char[] chars = str.toCharArray();
            chars[0]-=32;
            return String.valueOf(chars);
        }
        return null;
    }
}
