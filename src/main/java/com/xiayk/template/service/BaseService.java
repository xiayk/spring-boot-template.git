package com.xiayk.template.service;

import com.xiayk.template.common.exception.BaseException;
import com.xiayk.template.common.exception.EntityNotFountException;
import com.xiayk.template.common.utils.ReflectionUtils;
import com.xiayk.template.common.utils.SpringContextHelper;
import com.xiayk.template.common.utils.StringUtil;
import com.xiayk.template.entity.param.BaseQuery;
import com.xiayk.template.entity.po.BaseEntity;
import com.xiayk.template.common.utils.JpaHelp;
import com.xiayk.template.repository.BaseRepository;
import org.springframework.data.domain.Page;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

public interface BaseService<T extends BaseEntity, R extends BaseRepository<T, String>> {

    String ENTITY_NOT_FOUND_MESSAGE = "Entity not found !";

    /**
     * applicationContext中的bean注入
     * @return repository
     */
    default R baseRepository(){
        try {
            //获取<R> 泛型的Class
            Type type = ((ParameterizedTypeImpl) getClass().getInterfaces()[0].getGenericInterfaces()[0]).getActualTypeArguments()[1];
            Class<R> rClass = null;
            if (type instanceof Class){
                rClass = (Class<R>) type;
            }
            return SpringContextHelper.getBean(rClass);
        }catch (Exception e){
            throw new BaseException("注入application后重试");
        }
    }

    /**
     * 通过repositoryClass获取repositoryBean
     * @param repository
     * @return repository
     */
    default R baseRepository(Class<R> repository){
        return SpringContextHelper.getBean(repository);
    }


    default R baseRepository(R repository){
        return repository;
    }

    /**
     * 分页查询
     * @param query 继承BaseQuery
     * @return Page<T>
     */
    default <Q extends BaseQuery> Page<T> page(Q query){
        return baseRepository().findAll((Specification<T>)(root, criteriaQuery, criteriaBuilder) -> JpaHelp.query(root, query, criteriaBuilder), query.getPage());
    }

    /**
     * 分页查询
     * @param var1
     * @param pageable
     * @return
     */
    default Page<T> page(Specification<T> var1, Pageable pageable){
        return baseRepository().findAll(var1, pageable);
    }

    default <Q extends BaseQuery> Long count(Q query){
        return baseRepository().count((Specification<T>)(root, criteriaQuery, criteriaBuilder) -> JpaHelp.query(root, query, criteriaBuilder));
    }

    default T get(String var){
        return baseRepository().findById(var).orElse(null);
    }

    default T getNotNull(String var, String nullErrorMessage){
        return baseRepository().findById(var).orElseThrow(() -> new EntityNotFountException(Optional.ofNullable(nullErrorMessage).orElse(ENTITY_NOT_FOUND_MESSAGE)));
    }

    default T save(T var){
        return baseRepository().save(var);
    }

    default T saveSelect(T var){
        T var1 = this.getNotNull(var.getId(), ENTITY_NOT_FOUND_MESSAGE);
        ReflectionUtils.getClassAttr(var).forEach((k, v) -> {
            if (v != null){
                ReflectionUtils.invoke(var1, "set" + StringUtil.firstLetterToUpperCase(k), v);
            }
        });
        return this.baseRepository().save(var1);
    }

    default List<T> saveAll(List<T> data){
        return baseRepository().saveAll(data);
    }

    default void del(String id){
        baseRepository().deleteById(id);
    }

    default void delAll(List<String> vars){
        vars.forEach(id -> {
            baseRepository().deleteById(id);
        });
    }

    default List<T> list(List<String> vars){
        return baseRepository().findAllById(vars);
    }

    default List<T> list(){
        return baseRepository().findAll();
    }

    default List<T> listSort(Specification<T> var1, Sort sort){
        if (var1 == null){
            return baseRepository().findAll();
        }
        return baseRepository().findAll(var1, sort);
    }
}
